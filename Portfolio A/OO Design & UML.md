# OO Design & UML

## Static UML - Component Diagram

![image](Images/ComponentDiagram3.png)

The component diagram above gives a high-level overview of how different units interact and work with each other within the application. The component diagram was chosen as a method of representation because it clearly shows the applications external dependencies (the database and the user’s entry) and was deemed the most suitable level of abstraction to effectively communicate the high-level functionality of the application.

The **User Input** component is the part of the application that accepts the user’s input string and transforms it into key words. The **SQL Formation** component then takes these key words as an input and outputs the relevant SQL statement required to achieve the desired results. This is then inputted to the **Database** component. The SQL is used to search the information in the database and the component then returns results which fit the criteria. These results are returned to the **Result Viewing** component which then displays the results of the user’s initial entry in a human-readable format.

The search functionality is represented in the diagram as two different components (namely User Input and SQL Formation) contained in a subsystem classifier called Search Function. This is because by itself, the user input cannot search the database and must be used in conjunction with SQL in order for the search to behave.

The product is a client-server application as shown by the Client subsystem encompassing both the Search Function subsystem and the Result Viewing component. The Database component is then the server side of the application.

The database component does not require the SQL statement in order to function, therefore the SQL is not a required interface for the database. This is shown in the diagram using a non-solid dependency arrow. However, the SQL statement does require the database in order to be used.

The user sees the results of the query, so the Result Viewing component requires the database as illustrated by the “ball in socket” connection between the two components. However, there will be some intermediate steps required to get the results into the user’s view.

## Dynamic UML - Sequence Diagram

![image](Images/sequencediagram.png)

This sequence diagram illustrates the order in which parts of application will be used in collaboration and specifically focuses on communication and the sequence of any messages exchanged between them. The dynamic nature of this type of diagram shows more clearly than any static method (such as the component diagram) how components interrelate and how their behaviors change in response to requests or otherwise evolve over time.