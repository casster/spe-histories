# Overview

## Our Client

Our client is Mark Pajak, Head of Digital for Bristol Museums, a publicly funded offshoot of Bristol Council. Bristol Museums run various educational sites around Bristol such as Bristol Museum, M-Shed and the Bristol Archives.   

## The Problem

Bristol Museums are in possession of a collection of MP3 files and their respective transcripts, all broadly relating to the history of Bristol. These currently reside on their EMu Collections Management System, a specialist database tool used by history institutions to catalogue artefacts and store multimedia

In the present state, only individuals with internal access to the database can listen to the MP3s. Bristol Museums are not currently able to provide a service to allow external, interested parties access to their MP3s. It is also difficult for those with access to the database to find specific files because of unsuitable data fields and limited sorting mechanisms. 

Our client requires a tool that will allow users to quickly and easily access their collection of MP3s and respective transcripts, without having to be spatially located at the museum. 

## Our Vision

We want to build a web application that will allow users to search a database for an MP3, listen to it, and read its transcript. Users will be able to search by title, or by release date of the MP3. The user will then get a view all the relevant results and will be able to choose the specific file they wish to listen to. 

Initial scoping research has shown that EMu is not the most suitable database to query. Therefore, the information regarding the MP3s will have to be hosted on another database with a more sophisticated API. 

Subject to the museum having the infrastructure required to host another website, the web application will be hosted on their servers, with their domain. Otherwise, the application will have to be hosted elsewhere. 

The application will be developed using Spring Boot, therefore reducing the quantity of setup required for creating a web application. Java will be the main development language for the back-end, SQL will be used to query the database, and JavaScript, HTML, FreeMarker and CSS will be used in the development of the front-end environment.

Possible extensions to the application could include the development of more creative ways to visualise the data outputted after a search query is made. Initially this will display in a list format, similar to a Google search. Our client has suggested alternative formats such as the use of a map to display where the MP3s were recorded, a timeline view of the MP3s, or the grouping of the MP3s by subject. 