# Product Evaluation

Product evaluation was undertaken during the later iterations of the prototype. The majority of this was in the form of user testing followed by interview questions. We chose testers to be non-coding students from within the university. They were asked to try out the prototype MP3 search engine and then complete an interview in which they answered a set of pre-determined questions around the design and ease of use of the application. Some sample questions and answers have been detailed in the table below. The results of the test were recorded as paraphrases and not taken verbatim.

The questionnaire focused more holistically on user experience rather than the specific “historian” use case functionality with the key performance goal at this stage being to provide ease of use and simplicity.

| Sample questions | Sample answers |
|------------------|----------------|
| Is the search engine self-explanatory? | It is easy to get the search results, but there is too much information on the result screen to be able to pick out what you want. |
| Do the search results match your expectation? | The list format is easy to understand, but I would put in more information like date and reduce the amount of block text. |
| How difficult was it to navigate the engine? | It’s more difficult than it needs to be. I would literally rather just alter the query in the URL line instead of having to go back to the homepage to change or do a new search. |

A key finding from the interview and testing process was that the prototype search screen was underdeveloped in respect to its functionality. A notable deficiency was the lack of “back” button in the search screen. This meant that the user had to manually navigate back to the home page in order to make another search, adding both time and complexity to their experience. 

The fact that the browser “back” button could be used as an alternative return method (and was employed without though by several testers) meant that this deficiency only appeared in a few interview responses. Although not ideal, the fact that there was some method of returning meant this issue was determined to be low priority in comparison to other more urgent functional requirements that were still needing to be met.

A more major problem was found to be the quantity of text that displayed on the results screen, making the page look crowded and making it more difficult for users to spot the information they needed. The MP3 file titles provided by the client were determined to be uninformative. This resulted in the initial inclusion of the summary text field in our search results to help the user to be able to find the specific file they are looking for. However, after the length of this additional text was repeatedly flagged as a shortcoming, the product was evolved to cut off the summary after a certain word count and providing “read more” button functionality for each result. Our testers confirmed that this development resulted in a clear readability improvement.

The “read more” button provided underneath each search result navigates to a new page as it was found during testing that the creation of a drop-down section meant that users would have to manually reduce the expanded section, meaning they quickly recreated the original readability problem.

Other issues revealed from the interviews revolved around bugs that were able to be fixed in later development testing. There were several reports of the publication data not behaving correctly with the search engine showing all files in the database when a publication data was queried. This was a high priority issue that was fixed accordingly.

Evaluation has shown that we achieved most of our specific functional development requirements however due to problems accessing the museum MP3 API (deemed out of our sphere of influence) the product was not able to access the MP3 file for the user to listen to it. This is a failure of several of our key user stories surrounding users listening to the MP3 files and being able to listen to them outside of the museum.

However, the final product had a database of MP3s was fully searchable and all metadata about the MP3s was available to be viewed.