# Requirements

## Stakeholders

The stakeholders of our project are all the parties (both individuals and organisations) that have an interest in, or are impacted by, the outcome of this project. Each stakeholder has been presented using user stories which document these interests and impacts. 

### Mark Pajak

Mark, the client, is a stakeholder of the project. Having observed the initial problem and realised the need for the solution, he is invested in the success of the project both because it will make work easier for him and his colleagues, and because it allows information about Bristol to be more widely available, fulfilling his underlying aim of providing education. The user stories are:  

+ “As the client of the project, I want the museum’s repository of MP3 files to be stored in a database in a more structured format, so that it is easier to search through them.” 
+ “As the client of the project, I want the database of MP3 files to be available for everyone, so that interested parties can learn more about the history of Bristol.” 
+ “As the client of the project, I want the MP3 files to be able to be played on computers through the browser, so that users can listen to content in their own home, without needing to come to the museum and use its equipment.” 

### Bristol Museums

Mark works for Bristol Museums. Key objectives of the institution revolve around the accurate recording and preservation of artefacts, and the provision of learning experiences and resources to institutes and to the public. This project will facilitate both objectives, making the institution an interested party and stakeholder. The user stories are: 

+ “As the owner of Bristol Museums, I want the database of MP3 files to be widely accessible, increasing our user base and allowing more people to listen to them and learn about the history of Bristol.” 
+ “As the owner of Bristol Museums, I want the database of MP3 files to have a positive user experience so that museum staff are able to do their jobs more efficiently and with higher job satisfaction”
+ “As the owner of the Bristol Museum, I want the database of MP3 files to have a positive user experience, reflecting well on the museum and meaning external individuals are more likely to return and be repeat users.”
+ “As the owner of the Bristol Museum, I want this project to succeed, as the creation of more versatile information platforms increases user base and therefore potential donations.” 

### Speakers in the MP3s

The MP3 files are of real people speaking about their individual experiences. Their spoken word will be in the public domain, therefore the speakers (and their families) have an interest in ensuring that any personal data is protected as much as possible and that the integrity and context of any information they provide is maintained. The user stories are:

+ “As the speaker in an MP3 file, I want my recording to be available for anyone to listen to, so that they can learn about the history of Bristol.”
+ “As the speaker in an MP3 file, I want my personal information to be protected, so that I can choose to maintain my privacy and prevent the possibility of malicious actions against my person in response to a controversial opinion.”
+ “As the speaker in an MP3 file, I want to be able to have my recording removed at any time, so that my privacy can be protected if I wish.”

### Historians

Historians are the primary users of the application and are interesting in being able to perform their day-to-day work quickly, easily and accurately. The user stories are:  

+ “As a historian, I want to be able to search the museum’s repository of MP3 files, so that I can learn more about the history of Bristol.” 
+ “As a historian, I want to be able to read the transcripts of the MP3 files, so that I can accurately reproduce information in my research.” 
+ “As a historian, I want to be able to listen to the MP3 files, so that I can study how the Bristol accent has evolved over the years if that is part of my research.”

### People interested in the history of Bristol

Anyone who is interested in the history of Bristol has a stake in the success of this project as they are interested in being able to expand and improve their knowledge.

+ “As a person interested in the history of Bristol, I want to be able to search the MP3 files, so I can learn more about the city, from people who actually lived here.” 
+ “As a person interested in the history of Bristol, I want to be able to search the MP3 files, so I can compare Bristol now to how it was before.”
+ “As a person interested in the history of Bristol, I want more people to become interested in the history of Bristol, so that potentially more tourists will come, and Bristol can earn money from them and make the city better.”

## Use Case Diagram

![image](Images/usecase.png)

The use case diagram above shows the process users will follow when using the application.

The user stories of the historians were judged to be the most important and significant. Although other stakeholder user stories are meaningful, their underlying themes strongly correlate. Historians (and more generally, people interested in Bristol’s history) are the party who will be using this product most frequently. 

## Flow Steps

These are the flow steps that a user would take when using the system.

### Basic Flow

1. A user needs access to an MP3 file 
2. They go to the home page of the application 
3. They search for the title of the MP3 file they want 
4. They are then presented with the MP3 file they searched for 
5. They choose to see more information about the MP3 file 
6. They can read the summary and transcript of the MP3 
7. They can listen to the audio of the MP3 

### Alternative Flow

1. User is not sure exactly which MP3 they want to access 
2. They search with some vague criteria, such as a word or data 
3. They are presented with several MP3s that fit that search criteria 
4. They choose which MP3 they want specifically 
5. They can read the summary and transcript of the MP3 
6. They can listen to the audio of the MP3 

### Exeptional Flow

1. User tries to search for a MP3 with very constraining requirements 
2. There is no MP3 associated with their search criteria 
3. They are presented with a message saying that no MP3 could be found 
4. They have the option to search again 

## Functional Requirements

Based on the user stories given previously, the functional requirements are specific, feature-based, behaviour-defining requirements that must be met in some capacity for the product to be considered successful.

1. The framework used to build the project must be Spring Boot 
2. The users must be able to search the database of MP3 files 
	1. The users must be able to search by keywords in the title 
	2. The users must be able to search by keywords in the description 
	3. The users must be able to search by keywords in the transcripts 
	4. The users must be able to search by production date of the MP3 
3. The users must be able to view the results of their search 
4. The users must be notified if their search came back with no results 
	1. The users must be able to search the database again after each search 
5. The users must be able to see the metadata of the MP3 files that they have searched for 
	1. The users must be able to see the title of the MP3 file 
	2. The users must be able to see the description of the MP3 file 
	3. The users must be able to see the transcript of the MP3 file 
6. The users must be able to listen to the MP3 in browser 
	1. The users must be able to play the MP3 
	2. The users must be able to pause the MP3 at any point 
	3. The users must be able to resume the MP3 after they have paused it 
7. The users must be able to go back to their search results after listening to an MP3

## Non-functional Requirements

Based on the user stories given previously, the non-functional requirements are criteria or desirable features that could be used to judge the operation or improve the quality of the product. 

8. The project must be open source 
9. The project must have an MIT License 
10. The application must run on all web browsers 
	1. The application should be responsive to different screen sizes 
11. The application must be hosted on a server, so that anyone can use the application from anywhere 