# Development Testing

We took an assertion-centred approach to development testing during the implementation phases. A key objective during development was to avoid creating any unintended behaviours within both the web service and database.

We ensured that the most important user stories, as outlined in the requirements section, were tested as much as possible. One important user story to test is “as a historian, I want to be able to search the museum’s repository of MP3 files, so that I can learn more about the history of Bristol”. 

For the unit tests, we tested the entities and web layer separately. For the entities, a demo Transcript object was created. Then, using `ReflectionTestUtils`, we could test that the class was being created properly, and its fields could be accessed.

For the web layer, Spring Boot’s `MockMvc` class was used to test that all pages could be accessed and the content displayed on them was correct. We used methods such as `get()` to perform a mock GET request, `status()` to check the page response code, and `content()` to check the text on the page.

For integration testing, we were still considering the same user story, but this time we tested it by visiting the website and using it ourselves.

Challenges we faced during testing primarily revolved around the difficulties in creating expected outcomes. This was due to the sheer amount of data. Rather than creating a list of expected data given a keyword, we ultimately scanned results and checked whether results made sense. 

| What is being tested? | Input data | Expected outcome |
| ----------------------|------------|------------------|
| Home controller is not null | None - just auto-wire the home controller | The controller is injected successfully and is not null. |
| Transcripts class `irn` field | 1234 | When "1234" ia set as the `irn`, "1234" is returned from the getter. |
| Transcripts class `Nar_Title` field |"Title text" | When "Title text" is set as the `Nar_Title`, "Title text" is returned from the getter. |
| Transcripts class `Nar_Narrative` field | "Lorem ipsum" | When "Lorem ipsum" is set as the `Nar_Narrative`, "Lorem ipsum" is returned from the getter. |
| Transcripts class `Nar_Narrative_Summary` field | "the quick brown fox" | When "the quick brown fox" is set as the `Nar_Narrative_Summary`, "the quick brown box" is returned from the getter. |