# Client Documentation

## Deployment Instructions

Use maven to package the application and java -jar target/histories-0.0.1-SNAPSHOT.jar to run the application. Application is accessed from port 8080. 

## License Documentation

Copyright 2020 <COPYRIGHT HOLDER>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

## Storing and Accessing Data

The MP3 file metadata was stored in a MySQL database. The MP3 file itself would have been accessed via a HTTP request using the MP3’s ID, however the API we were given to use never worked.

We chose to use a MySQL database because MySQL Workbench is free, which allows us to easily add the data to the database. The database was hosted by Amazon Web Services. We used Amazon Web Services for our database allowing us to use $300 of free credits. When the credits expired, a H2 database with dummy data was used to create the application summary video for marking purposes.

The information required to access the data in our application (the URL, username and password for the database) are provided in the `application.properties` file.

## Meta Application Features

The application was hosted on an Amazon Web Services Elastic Cloud Compute Instance. Specifically, a 1TB Ubuntu server. The credits for the server have now expired.