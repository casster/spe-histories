<!DOCTYPE html>
<html>
    <head>
        <title>Results</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <style>
            p,a{
                font-family: Arial, Helvetica, sans-serif;
            }
            div{
                width: 75%;
                margin-left: auto;
	            margin-right: auto;
	            max-width: 1000px;
	            float: none;
                padding-top: 15px;
                padding-bottom: 15px;
            }
            .title{
                font-weight: bold;
                font-size: 20px;
            }
            .readMore{
                font-size: 15px;
                color: crimson;
            }
        </style>
    </head>
    <body>
    <center><p><button onclick="goBack()">Go back</button><p></center>
    <center><p><input type="text" id="userInput" placeholder="Search..."></input></p></center>
    <center><p><button onclick="submit()">Submit</button> </p></center>

     <script>
        function goBack(){
            window.location.href = "/";
        }
        
        function submit(){
            var userInput = document.getElementById("userInput").value;
            window.location.href = "/getRecords?i="+userInput;
        }

        function readMore(s){
            console.log(s);
            window.location.href = "/record?i="+s;
        }
    </script>
    <#list mp3 as m>
        <div>
            <#assign title>${m.nar_Title}</#assign>
            <!--<p>Irn = ${m.irn!"Not found"}</p>-->
            <p class = "title">${m.nar_Title!"Not found"}</p>
            <!--
            Only displays the first 400 characters of the summary
            -->
            <#assign summary>${m.nar_Narrative_Summary}</#assign>
            <#if summary?length &lt; 400>
            <p class = "summary">${m.nar_Narrative_Summary!"Not found"}</p>
            <#else>
            <p class = "summary">${m.nar_Narrative_Summary?substring(0,400)!"Not found"}...</p>
            </#if>
            <a href="/record?i=${title}" class = "readMore">Read more...</a>
        </div>
    </#list>
    </body>
</html>