<!DOCTYPE html>
<html>
    <head>
        <title>Results</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <style>
            p,a{
                font-family: Arial, Helvetica, sans-serif;
            }
            div{
                width: 75%;
                margin-left: auto;
	            margin-right: auto;
	            max-width: 1000px;
	            float: none;
                padding-top: 15px;
                padding-bottom: 15px;
            }
            .title{
                font-weight: bold;
                font-size: 20px;
            }
        </style>
    </head>
    <body>
    <center><p><button onclick="goHome()">Go home</button><p></center>
    <center><p><input type="text" id="userInput" placeholder="Search..."></input></p></center>
    <center><p><button onclick="submit()">Submit</button> </p></center>

    <script>
        function goHome(){
            window.location.href = "/";
        }
        function submit(){
            var userInput = document.getElementById("userInput").value;
            window.location.href = "/getRecords?i="+userInput;
        }
    </script>
        <#list mp3 as m>
        <div>
            <p class = "title">${m.nar_Title!"Not found"}</p>
            <p class = "summary">${m.nar_Narrative_Summary!"Not found"}</p>
            <p class = "transcript">${m.nar_Narrative!"Not found"}</p>
        </div>
        </#list>
    </body>
</html>