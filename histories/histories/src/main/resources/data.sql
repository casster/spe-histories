
INSERT INTO transcript (irn,Nar_Title,Nar_Narrative_Summary,Nar_Narrative) VALUES
    (1,'Going for a walk','Bob goes for a walk','Bob: Yeah I had a good time'),
    (2,'Living in Bristol','Alice talks about living in Bristol','Alice: It is nice'),
    (3,'Working in Bristol','Charlie talks about working in Bristol','Charlie: I work in Tesco'),
    (4,'Wills Memorial Building','David talks about Wills Memorial Building','David: It is an old building'),
    (5,'Being a uni student','Ethan talks about being a student at UoB','Ethan: I do computer science');