package com.webservice.histories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MP3Repository extends CrudRepository<MP3, Integer> {

    //Selects irns that contain i
    @Query("SELECT m FROM MP3 m WHERE irn LIKE CONCAT('%',:i,'%')")
    List<MP3> findMP3withIrn(@Param("i") Integer i);

    @Query("SELECT m FROM MP3 m where Mul_Description LIKE CONCAT('%',:i,'%')")
    List<MP3> findMP3withdesc(@Param("i") String i);
}