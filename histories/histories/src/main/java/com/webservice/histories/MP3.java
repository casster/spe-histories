package com.webservice.histories;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


//The class for one of the tables.
@Entity @Table(name = "mp3")
public class MP3 {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer irn;

    private Integer Mul_Multi_Media_Ref_key;

    private String enarratives_key;

    private String Mul_Identifier;

    private String Rig_Acknowledgement;

    private Boolean Adm_Publish_Web_No_Password;

    private Integer Rights_IRN;

    private Integer MUL_WEB_YES;

    private String Mul_Description;

    private String Mul_Mime_Type;

    public Integer getIrn(){
      return this.irn;
    }

    public Integer getMul_Multi_Media_Ref_key(){
      return this.Mul_Multi_Media_Ref_key;
    }

    public void setMul_Multi_Media_Ref_key(Integer n){
        this.Mul_Multi_Media_Ref_key = n;
    }

    public String getEnarratives_key(){
        return this.enarratives_key;
    }
  
    public void setEnarratives_key(String n){
        this.enarratives_key= n;
    }

    public String getMul_Identifier(){
        return this.Mul_Identifier;
    }
  
    public void setMul_Identifier(String n){
        this.Mul_Identifier= n;
    }

    public String getRig_Acknowledgement(){
        return this.Rig_Acknowledgement;
    }
  
    public void setRig_Acknowledgement(String n){
        this.Rig_Acknowledgement= n;
    }

    public Boolean getAdm_Publish_Web_No_Password(){
        return this.Adm_Publish_Web_No_Password;
    }
  
    public void setAdm_Publish_Web_No_Password(Boolean n){
        this.Adm_Publish_Web_No_Password= n;
    }

    public Integer getRights_IRN(){
        return this.Rights_IRN;
    }
  
    public void setRights_IRN(Integer n){
        this.Rights_IRN= n;
    }

    public Integer getMUL_WEB_YES(){
        return this.MUL_WEB_YES;
    }
  
    public void setMUL_WEB_YES(Integer n){
        this.MUL_WEB_YES= n;
    }

    public String getMul_Description(){
        return this.Mul_Description;
    }
  
    public void setMul_Description(String n){
        this.Mul_Description= n;
    }

    public String getMul_Mime_Type(){
        return this.Mul_Mime_Type;
    }
  
    public void setMul_Mime_Type(String n){
        this.Mul_Mime_Type= n;
    }


}