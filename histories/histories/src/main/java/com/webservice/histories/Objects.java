package com.webservice.histories;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

//class for transcript table
@Entity @Table(name = "objects")
public class Objects {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)

    private Integer irn;
    private String Col_Object_Number;
    private String Simple_Name;
    private String Object_Published_To_Web;
    private Integer irn2;
    private String Production_Date;

    public Integer getIrn() {return this.irn;}

    public String getCol_Object_Number() {return this.Col_Object_Number;}
    public void setCol_Object_Number(String n) {this.Col_Object_Number = n;}

    public String getSimple_Name() {return this.Simple_Name;}
    public void setSimple_Name(String n){this.Simple_Name = n;}

    public String getObject_Published_To_Web() {return this.Object_Published_To_Web;}
    public void setObject_Published_To_Web(String n) {this.Object_Published_To_Web = n;}

    public Integer getIrn2() {return this.irn2;}

    public String getProduction_Date() {return this.Production_Date;}
    public void setProduction_Date(String n) {this.Production_Date = n;}
}