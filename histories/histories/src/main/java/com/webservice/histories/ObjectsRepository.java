package com.webservice.histories;

import org.springframework.data.repository.CrudRepository;


// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

//This is supposed to be empty.

public interface ObjectsRepository extends CrudRepository<Objects, Integer> {

}