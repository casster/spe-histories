package com.webservice.histories;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HistoriesApplication {

	public static void main(String[] args) {
		SpringApplication.run(HistoriesApplication.class, args);
	}

}
