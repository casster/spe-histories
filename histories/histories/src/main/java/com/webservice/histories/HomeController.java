package com.webservice.histories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class HomeController {

    @Autowired
    private TranscriptsRepository transRepo;

    @GetMapping("/")
    public String home() {
        return "home";
    }

    @RequestMapping("/getRecords")
    public String getRecords(Model model, @RequestParam("i") String i){
        Iterable<Transcripts> result = transRepo.findMP3withdesc(i);
        model.addAttribute("mp3", result);
        return "showRecords";
    }

    //returns all transcript table info to screen
    @GetMapping(path="/trans")
    public String getAllTrans(Model model) { 
        Iterable <Transcripts> results = transRepo.findAll();
        model.addAttribute("mp3", results);
        return "showRecords";
    }

    @GetMapping("/record")
    public String showOneRecord(Model model,@RequestParam("i") String i){
        Iterable<Transcripts> result = transRepo.findMP3withdesc(i);
        model.addAttribute("mp3", result);
        return "showRecord";
    }
}

