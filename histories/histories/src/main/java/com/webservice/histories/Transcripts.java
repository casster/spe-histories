package com.webservice.histories;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

//class for transcript table
@Entity @Table(name = "transcript")
public class Transcripts {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer irn;
    private String Nar_Title;
    private String Nar_Narrative;
    private String Nar_Narrative_Summary;

    public Integer getIrn() {return this.irn;}

    public String getNar_Title() {return this.Nar_Title;}
    public void setNar_Title(String n) {this.Nar_Title = n;}

    public String getNar_Narrative() {return this.Nar_Narrative;}
    public void setNar_Narrative(String n) {this.Nar_Narrative = n;}

    public String getNar_Narrative_Summary() {return this.Nar_Narrative_Summary;}
    public void setNar_Narrative_Summary(String n) {this.Nar_Narrative_Summary = n;}
}
