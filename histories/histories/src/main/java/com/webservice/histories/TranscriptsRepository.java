package com.webservice.histories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;


public interface TranscriptsRepository extends CrudRepository<Transcripts, Integer> {

    @Query("SELECT t FROM Transcripts t WHERE nar_title LIKE CONCAT('%',:i,'%')")
    List<Transcripts> findMP3withdesc(@Param("i") String i);

}