package com.webservice.histories;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.hamcrest.Matchers.containsString;

@SpringBootTest
@AutoConfigureMockMvc
public class WebLayerTest {
    @Autowired
	private MockMvc mockMvc;

	@Test
	public void testHomePage() throws Exception {
		this.mockMvc.perform(get("/")).andExpect(status().isOk());
    }
    @Test
    public void testRecordsPage() throws Exception {
		this.mockMvc.perform(get("/getRecords?i=Bristol")).andExpect(status().isOk()).andExpect(content().string(containsString("Bristol"))).andExpect(content().string(containsString("Alice talks about living in Bristol")));
    }

    @Test
    public void testSingleRecord() throws Exception{
        this.mockMvc.perform(get("/record?i=Wills Memorial Building")).andExpect(status().isOk()).andExpect(content().string(containsString("David talks about Wills Memorial Building"))).andExpect(content().string(containsString("David: It is an old building")));
    }
}