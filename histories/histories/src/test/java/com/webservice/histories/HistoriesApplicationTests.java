package com.webservice.histories;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.util.*;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class HistoriesApplicationTests {
	@Autowired
	private HomeController controller;

	@Test
	void contextLoads() {
		assertThat(controller).isNotNull();
	}

	@Test
	void transcriptClassWorks() {
		Transcripts t = new Transcripts();
		ReflectionTestUtils.setField(t, "irn", 1234);
		ReflectionTestUtils.setField(t, "Nar_Title", "Title text");
		ReflectionTestUtils.setField(t, "Nar_Narrative", "Lorem ipsem");
		ReflectionTestUtils.setField(t, "Nar_Narrative_Summary", "the quick brown fox");

		assertTrue(t.getIrn().equals(1234));
		assertTrue(t.getNar_Title().equals("Title text"));
		assertTrue(t.getNar_Narrative().equals("Lorem ipsem"));
		assertTrue(t.getNar_Narrative_Summary().equals("the quick brown fox"));
	}

}
